#include <iostream>
#include <string>

using namespace std;

string buildName(string name, string prefix = "Mr.", string suffix = "Jr.")
{
	string result;
	auto it = result.begin();
	result.insert(it, prefix.begin(), prefix.end());
	result.append(name);
	result.insert(result.end(), ' ');
	result.append(suffix);
	return result;
}

string replace(const string& s, const string& oldVal, const string& newVal)
{
	string result = s;
	for (string::size_type i = 0; i < s.size(); ++i) {
		if (result.substr(i, oldVal.length()) == oldVal) {
			result.erase(i, oldVal.length());
			result.insert(i, newVal);
			i += (newVal.length() - 1);
		}
	}
	return result;
}

int main(int /*argc*/, char* /*argv*/ [])
{
	cout << replace("C++ is a a simple language!", "a ", "a very ") << endl;
	cout << buildName("Martein") << endl;
}
