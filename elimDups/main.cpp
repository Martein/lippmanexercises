#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif

bool check_size(const std::string& str, std::size_t n) {
    return str.size() > n;
}

auto greaterThanFiveSymbols(std::string& s) -> bool {
    return s.size() >= 5;
}

auto isShorter(std::string& s1, std::string& s2) -> bool {
    return s1.size() < s2.size();
}

auto elimDups(std::vector<std::string>& refSVec) -> void {
    std::sort(refSVec.begin(), refSVec.end(), isShorter);
    refSVec.erase(std::unique(refSVec.begin(), refSVec.end()));
}

int main(int /*argc*/, char* /*argv*/ []) {
#if defined(_WIN32) || defined(_WIN64)
    SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);
#endif
    std::vector<std::string> family = {
        "Vitalina", "Eugene", "Artur", "Estell", "Artur", "Ira", "Dasha"};

    elimDups(family);
    std::stable_sort(family.begin(), family.end());

    int n = std::count_if(family.begin(),
                          family.end(),
                          std::bind(check_size, std::placeholders::_1, 5));

    std::cout << n << std::endl;

    setlocale(LC_ALL, "ru_RU.UTF-8");
    std::cout << ""
              << "привет\n";

    return 0;
}
