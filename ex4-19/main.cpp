#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

int main()
{
  vector<int> ivec = {1, 2, 3, 4, 0};
  int* ptr = &ivec[0];
  if (*ptr++ && *ptr) cout << "equal" << endl;
  return 0;
}

