#include <iostream>
#include <vector>

int main() {
  std::vector<int> ivec1 = { 0, 1, 1, 2 }, ivec2 = { 0, 1, 1, 2, 3, 5, 8 };
  decltype(ivec1.size()) cnt = 0;

  for (auto iter1 = ivec1.begin(), iter2 = ivec2.begin();
       (iter1 != ivec1.end()) && (iter2 != ivec2.end()) && (*iter1 == *iter2);
       ++iter1, ++iter2, ++cnt) {
  }

  if (cnt > 0)
    std::cout << "First vector is prefix of second vector." << std::endl;
  else
    std::cout << "First vector isn't prefix of second vector." << std::endl;

  std::vector<int> ivec3 = { 1, 2, 3, 4, 5, 6 };
  for (auto& i : ivec3) {
    std::cout << (i *= 2) << std::endl;
    std::cout << i << " ";
  }

  return 0;
}
