#include <iostream>
#include <string>

int main() {
  std::string last_word = {};
  std::string word = {};
  while (true) {
    if (std::cin >> word) {
      if (last_word == word)
        break;
      last_word = word;
    } else {
      last_word = "";
      break;
    }
  }
  if ((!word.empty()) && (!last_word.empty()) && (last_word == word))
    std::cout << "��������饥 ᫮��: " << word << std::endl;
  else
    std::cout << "���������� ᫮� ���." << std::endl;

  return 0;
}
