#include <iostream>

using namespace std;

auto foo(int) {}

auto foo(auto) -> decltype(foo(5)) {

}

class Func {
public:
    Func(auto a...) {
        va_list argList;
    }
    bool operator()(const string& s) const {
        cout << s << ' ' << endl;
    }
};

int main()
{
    Func f(6, 6, "str", 5.6);
    //f("hello");
    return 0;
}
