#include <iostream>
#include <vector>

void debug_output(const char* msg)
{
    std::cout << msg << std::endl;
}

std::vector<int>::iterator findNumber(std::vector<int>& ivec)
{
    std::cout << "Введите число, которое необходимо искать в векторе: ";

    int k; // k будет хранить искомое число
    std::cin >> k;

    std::vector<int>::iterator end_it = ivec.end();
    std::vector<int>::iterator res_it;
    std::vector<int>::iterator cur_it = ivec.begin();
    bool found = false; // found - признак "найден/не найден"

    while (cur_it != end_it) {
        if (*cur_it == k) {
            found = true; // число найдено, запоминаем это
            res_it = cur_it;
            break; // прерываем цикл поиска числа
        }
        cur_it++;
    }
    if (!found) {
        std::cout << "Такого числа нет! Получи ошибку!\n";
        throw;
    }
    std::cout << "Такое число есть, да.\n";
    return res_it;
}

int main(int /*argc*/, char* /*argv*/ [])
{
    std::vector<int> a;
    a.push_back(1);
    a.push_back(2);
    a.push_back(3);
    a.push_back(4);
    a.push_back(5);
    auto it = findNumber(a);
    a.pop_back();
    a.pop_back();
    a.pop_back();
    a.pop_back();
    a.pop_back();
    a.pop_back();
    return 0;
}
