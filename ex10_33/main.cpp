#include <fstream>
#include <iostream>
#include <iterator>

using namespace std;

int main(int /*argc*/, char* argv[]) {
    ifstream in(argv[1]);
    if (in.is_open()) {
        std::ofstream         out1(argv[2]), out2(argv[3]);
        istream_iterator<int> in_iter(in), eof;
        ostream_iterator<int> out1_it(out1, " "), out2_it(out2, "\n");
        while (in_iter != eof) {
            if (*in_iter % 2) {
                *out1_it++ = *in_iter;
            } else {
                *out2_it++ = *in_iter;
            }
            ++in_iter;
        }
    } else {
        cout << "Input file not found!\n";
    }
    return 0;
}
