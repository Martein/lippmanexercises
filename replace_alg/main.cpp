#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main(int /*argc*/, char* /*argv*/[]) {
    //-------{ replace }--------------------------------------------------------
    std::vector<int> ivec1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::replace(ivec1.begin(),
                 ivec1.end(),
                 2,
                 6); // Заменяет число 2 в векторе на число 6
    std::cout << "\nThis is ivec1: \n";
    for (auto& v : ivec1)
        std::cout << v << " ";
    std::cout << std::endl;

    //-------{ replace_copy }---------------------------------------------------
    std::vector<int> res_ivec2, ivec2 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::replace_copy(ivec2.begin(),
                      ivec2.end(),
                      std::back_inserter(res_ivec2),
                      2,
                      6); // Заменяет в копии исходного вектора число 2 на 6.
    // Результат пишется в вектор res_ivec2.
    std::cout << "\nThis is ivec2-copy: \n";
    for (auto& v : res_ivec2)
        std::cout << v << " ";
    std::cout << std::endl;

    //-------{ replace_if }-----------------------------------------------------
    std::vector<int> ivec3 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::replace_if(ivec3.begin(),
                    ivec3.end(),
                    [](int v) { return (v % 2); },
                    6); // Заменяет в векторе числа, удовлетворяющие условию
                        // предиката на число 6.
    std::cout << "\nThis is ivec3: \n";
    for (auto& v : ivec3)
        std::cout << v << " ";
    std::cout << std::endl;

    //-------{ replace_copy_if }------------------------------------------------
    std::cout << "\nThis is ivec4-copy: \n";
    std::vector<int> res_ivec4, ivec4 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::replace_copy_if(ivec4.begin(),
                         ivec4.end(),
                         std::back_inserter(res_ivec4),
                         [](int v) { return v % 2; },
                         6); // Создаёт копию вектора ivec4, заменяя в ней
                             // нечётные числа на число 6.
    std::copy(res_ivec4.begin(),
              res_ivec4.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
