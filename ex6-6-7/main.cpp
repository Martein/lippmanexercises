//==============================================================================
#include <iostream>
#include "windows.h"
//==============================================================================

size_t foo(int arg) {
  void* pStartBody = &&body;
  void* pEndBody = &&end_body;
  size_t sizeOfModifiedCode = ((size_t)pEndBody - (size_t)pStartBody);

  DWORD oldProtection;
  VirtualProtect((LPVOID)pStartBody, sizeOfModifiedCode, PAGE_EXECUTE_READWRITE,
                 &oldProtection);

  for (std::size_t i = 0; i < sizeOfModifiedCode; ++i) {
    *((char*)(pStartBody) + i) = (char)0x90; // 90 - опкод инструкции NOP
  }
  FlushInstructionCache(GetCurrentProcess(), pStartBody, sizeOfModifiedCode);

body:
  std::cout << "Этот код никогда не будет выполнен." << std::endl;
  static size_t ctr = 9;
  if (ctr == 9) {
    ctr *= ctr;
    int local = 1;
    std::cout << "Parameter: " << arg << " Local: " << local << std::endl;
  }
end_body:
  return ++ctr;
}
//==============================================================================
int main() {
  std::cout << foo(5) << std::endl;
  return 0;
}
//==============================================================================
