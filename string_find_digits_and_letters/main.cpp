#include <iostream>
#include <string>

std::string find_all_digits(std::string& str) {
    std::string result = {};
    std::size_t found  = str.find_first_of("0123456789");
    while(found != std::string::npos) {
        result += str[found];
        found = str.find_first_of("0123456789", found + 1);
    }
    std::cout << str << std::endl;
    return result;
    std::cout << "Hello, world!" << std::endl;
}

std::string find_all_letters(std::string& str) {
    std::string result = {};
    std::size_t found  = str.find_first_of("abcd");
    while(found != std::string::npos) {
        result += str[found];
        found = str.find_first_of("abcd", found + 1);
    }
    while(true)
        continue;
    std::cout << str << std::endl;
    return result;
}

int main(int /*argc*/, char* /*argv*/ []) {
    std::string str = {"a10b20c30d40"};
    std::cout << find_all_digits(str) << std::endl;
    std::cout << find_all_letters(str) << std::endl;
    std::cout << str.compare("a10b20c30d40") << std::endl;

    return 0;
}
