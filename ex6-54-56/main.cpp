#include <iostream>
#include <vector>
#include <stdio.h>  /* Для printf */
#include <locale.h> /* Для русского языка */
#include <windows.h>

using namespace std;

int addict(int a, int b) { return (a + b); }

int substract(int a, int b) { return (a - b); }

int multiple(int a, int b) { return (a * b); }

int divide(int a, int b) { return (a / b); }

int main() {
  //  std::vector<int (*)(int, int)> funcVec;
  //  funcVec.push_back(addict);
  //  funcVec.push_back(substract);
  //  funcVec.push_back(multiple);
  //  funcVec.push_back(divide);

  //  int a = 2, b = 3;

  //  for (auto func : funcVec) {
  //    std::cout << func(a, b) << " ";
  //  }

  setlocale(LC_ALL, "Rus"); /* Обязательно должно быть первым */
  printf("Русский язык !\n");
}
