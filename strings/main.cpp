#include <iostream>
#include <string>
#include <cctype>
#include <cstring>
#include <thread>
#include <chrono>

int& foo(int& a) {
  return a;
}

int main() {
  char c_str1[] = {"Hello, world!"};
  char c_str2[] = {"Hello, world!"};
  ++c_str1[0];
  --c_str1[0];
  char c_str3[strlen(c_str1) + strlen(c_str2) + 1];
  if (std::strcmp(c_str1, c_str2) == 0) {
    std::cout << "C-strings are equal." << std::endl;
  } else {
    std::cout << "C-strings aren't equal." << std::endl;
  }
  std::string str1 = "Hello, world!", str2 = "Hello, world!";
  if (str1 == str2) {
    std::cout << "Strings are equal." << std::endl;
  } else {
    std::cout << "Strings aren't equal." << std::endl;
  }
  strcpy(c_str3, c_str1);
  strcat(c_str3, c_str2);
  std::cout << "Result string: " << c_str3 << std::endl;
  int ia[3][4] = {};
  int(*pia)[4] = &ia[2];
  for (auto& e : *pia) e = 1;
  for (auto& cols : ia) {
    for (auto rows : cols) std::cout << rows << " ";
    std::cout << std::endl;
  }
  int b = 1;
  foo(b) = 2;
  std::cout << b << std::endl;
  int* p = &b;
  decltype(*p) rb = b;
  rb = 3;
  std::cout << b << std::endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  return 0;
}
