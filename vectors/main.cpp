#include <vector>
#include <iostream>
#include <string>

using std::vector;
using std::string;
using std::cout;
using std::cin;
using std::endl;

int main() {
  vector<int> ivec;
  int n;
  while (cin >> n) ivec.push_back(n);
  for (auto it = ivec.begin() + 1; it != ivec.end(); ++it)
    cout << *(it - 1) + *it << endl;
  return 0;
}
