#include <iostream>
#include <stdexcept>

using namespace std;

void foo(int first, int second) {
  if (second == 0)
    throw logic_error("Division by zero.\n");
  cout << static_cast<double>(first) / second << endl;
}

int fact(int value) {
  int temp = value;
  if (value > 1)
    temp *= fact(--value);
  return temp;
}

// double abs(double value) {
//  if (value < 0)
//    value = -value;
//  return value;
//}

int main() {
  //  int a = 0, b = 0;
  //  cout << "Enter two integer values: ";
  //  while (cin >> a >> b) {
  //    try {
  //      foo(a, b);
  //    } catch (logic_error& le) {
  //      cout << "Enter correct values. ";
  //    }
  //    cout << "Enter two integer values: ";
  //  }
  cout << fact(5) << endl;
  cout << abs(-1) << endl;
  return 0;
}