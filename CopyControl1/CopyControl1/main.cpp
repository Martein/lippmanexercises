#include <iostream>

using namespace std;

class Base {
public:
	Base(int& refValue) 
	: m_refValue(refValue) {}
	~Base() = delete;
private:
	int m_refValue;
};

class Base2
{
public:
	Base2();
	Base2(int& i);
	~Base2();

private:
	Base m_base;
};

Base2::Base2()
{
}

Base2::Base2(int & i)
{
	m_base = i;
}

Base2::~Base2()
{
}

int main(int argc, char** argv) {
	int i = 2, j = 3;
	return 0;
}