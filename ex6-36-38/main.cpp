#include <iostream>
#include <string>
//------------------------------------------------------------------------------
std::string (&funcA(std::string (&aS)[10]))[10] {
  for (auto& str : aS) {
    str = "A";
  }
  return aS;
}
//------------------------------------------------------------------------------
using ArrStrT = std::string (&)[10];

ArrStrT funcB(ArrStrT aS) {
  for (auto& str : aS) {
    str = "B";
  }
  return aS;
}
//------------------------------------------------------------------------------
auto funcC(ArrStrT aS) -> std::string (&)[10] {
  for (auto& str : aS) {
    str = "C";
  }
  return aS;
}
//------------------------------------------------------------------------------
std::string arr1[10];
decltype(arr1)& funcD(ArrStrT aS) {
  for (auto& str : aS) {
    str = "D";
  }
  return aS;
}

//------------------------------------------------------------------------------
//==============================================================================
int main() {
  std::string arr[10] = { { "A" },
                          { "B" },
                          { "C" },
                          { "D" },
                          { "E" },
                          { "F" },
                          { "G" },
                          { "H" },
                          { "I" },
                          { "J" } };
  funcD(arr);
  for (auto str : arr)
    std::cout << str << " ";
  std::cout << std::endl << __func__ << std::endl;
  return 0;
}
//==============================================================================