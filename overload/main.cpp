#include <iostream>

class A {};

class B {};

class C : public A, public B {};

// void foo(const A a, int) {}
void foo(B b) {}

int main() {
  const C c;
  foo(c);
  std::cout << __FILE__ << std::endl;
  return 0;
}
