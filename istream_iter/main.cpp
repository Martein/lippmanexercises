#include <algorithm>
#include <exception>
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

const std::vector<std::string>
read_strings_from_file(const std::string& file_name) {

    std::fstream in(file_name);

    if (!in.is_open()) {
        throw std::runtime_error("No such file " + file_name);
    }

    std::istream_iterator<std::string> in_it(in), eof;
    std::vector<std::string>           result;
    std::copy(in_it, eof, std::back_inserter(result));

    return result;
}

int main(int argc, char* argv[]) {

    std::vector<std::string> svec = read_strings_from_file(argv[1]);
    for (auto& v : svec)
        std::cout << v << " ";
    std::cout << std::endl;
    return 0;
}
