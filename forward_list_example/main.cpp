#include <algorithm>
#include <forward_list>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

using namespace std;

class A {
public:
    A(int value = 0)
      : m_a(value)
      , m_a(value) {}

    void           foo() {}
    double         bar() {}
    string         foobar() {}
    vector<string> strVec();

private:
    int m_a;
};
//==============================================================================
void duplicate_elements(forward_list<int>& f_lst) {
    auto iter = f_lst.begin();
    auto prev = f_lst.before_begin();
    while (iter != f_lst.end()) {
        if (*iter % 2) {
            prev = f_lst.insert_after(iter, *iter);
            iter = prev;
            iter++;
        } else {
            iter = f_lst.erase_after(prev);
        }
    }
}
//==============================================================================
void foo(forward_list<string>& flst_s, string first, string second) {
    auto prev  = flst_s.before_begin();
    auto curr  = flst_s.begin();
    bool found = false;
    while (curr != flst_s.end()) {
        if (*curr == first) {
            curr  = flst_s.insert_after(prev, second);
            found = true;
        } else {
            prev = curr;
            ++curr;
        }
    }
    if (!found) flst_s.insert_after(prev, second);
}
//==============================================================================
void delete_odd(forward_list<int>& flst_i) {
    auto prev = flst_i.before_begin();
    auto curr = flst_i.begin();
    while (curr != flst_i.end())
        if (*curr % 2 != 0)
            curr = flst_i.erase_after(prev);
        else {
            prev = curr;
            ++curr;
        }
}
//==============================================================================
void output_flst_i(forward_list<int>& flst_i) {
    for (auto& v : flst_i) cout << v << " ";
    cout << endl;
}
//==============================================================================
int main(int /*argc*/, char* /*argv*/ []) {
    forward_list<int> flst_i = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    delete_odd(flst_i);
    duplicate_elements(flst_i);
    output_flst_i(flst_i);
    string str = "01234567890123456789012345678901234567890123456789";
    cout << str.capacity() << endl;
    forward_list<string> flst_s = {
        "C++", "is", "a", "simple", "progamm", "language!"};

    foo(flst_s, "lang", "Yea!");
    for (auto& str : flst_s) cout << str << " ";
    return 0;
} //============================================================================
