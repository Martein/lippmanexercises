#include <cmath>
#include <iostream>

using namespace std;

class B {
public:
    B(int init_value = 0)
      : m_value(init_value) {}

private:
    int m_value;
};

class A {
public:
    A() {}
    int  value() const;
    void setValue(int value);
    bool operator!=(bool value);

private:
    int m_value;
};

int A::value() const { return m_value; }

void A::setValue(int value) { m_value = value; }

bool A::operator!=(bool value) { return m_value % 2 != value; }

int main() {
    do {

    } while (false);
    A a;
    a.setValue(6);

    std::cout << a.value() << std::endl;
    if (a != false) {
        std::cout << "A!" << std::endl;
    }
    return 0;
}
