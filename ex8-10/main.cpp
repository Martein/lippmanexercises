#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int main(int /*argc*/, char* argv[])
{
    std::ifstream in(argv[1]);
    std::vector<std::string> lines;

    if (in.is_open()) {
        std::string line;
        while (std::getline(in, line)) {
            lines.push_back(line);
        }
        std::stringstream word;
        for (auto& str : lines) {
            //std::cout << str << std::endl;
            word << str;
            std::string w;
            while (word >> w)
                std::cout << w << std::endl;

            word.clear();
        }
    }
    else {
        std::cerr << "Can't open in file." << std::endl;
    }
    return 0;
}
