#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <vector>

int main(int /*argc*/, char* /*argv*/ []) {
    //    std::vector<int> ivec = {1, 2, 3, 4, 5, 6, 7};
    //    std::map<std::vector<int>::iterator, int> ivec_map;

    //    std::pair<std::map<std::vector<int>::iterator, int>::iterator, bool>
    //    ret =
    //    ivec_map.insert({ivec.begin(), int(*ivec.begin())});

    //    if (ret.second)
    //        std::cout << ret.first->second << std::endl;

    //    std::map<char, int> map1;
    //    map1['a'] = 10;
    //    map1['b'] = 20;
    //    map1['d'] = 30;
    //    map1['e'] = 40;

    //    auto lowit = map1.lower_bound('c');
    //    std::cout << (*lowit).first << std::endl;

    std::multimap<std::string, std::string> authors = {
        {"Stenly Lippman", "C++ Primer"},
        {"Stiven Prata", "C++ Primer"},
        {"Bruce Ekkel", "Philosofy of C++"},
        {"Scot Meyers", "Effective C++"},
        {"Scot Meyers", "Effective STL"}};

    auto author    = authors.find("Scot Meyers");
    size_t entries = authors.count("Scot Meyers");
    while (entries) {
        //std::cout << author->first << " : " << author->second << std::endl;
        author = authors.erase(author);
        --entries;
    }

    for (const auto& author : authors) {
        std::cout << author.first << " : " << author.second << std::endl;
    }
    return 0;
}
