#include <iostream>

using namespace std;

class A {
public:
    A() {}
    ~A() {}

private:
};

int main(int /*argc*/, char* /*argv*/[]) {
    A* a = new A;
    delete a;

    return 0;
}
