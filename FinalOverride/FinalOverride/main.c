int f(int x) {
	int a;
	if (x)
		a = 42;
	return a;
}

int main()
{
	const int i = 1;
	int a = i;
	f(0);
    return 0;
}
