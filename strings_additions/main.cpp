#include <iostream>
#include <string>
#include <vector>

int main(int /*argc*/, char */*argv*/[])
{
    std::vector<char> vc = {'H', 'e', 'l', 'l', 'o', '!'};
    std::string str(vc.begin(), vc.end());
    std::cout << str << std::endl;
    return 0;
}
