#include <exception>
#include <iostream>
#include <string>
#include <vector>

int sum_ints(const std::vector<std::string>& svec) {
    int result_sum = 0;
    for(auto& str : svec) {
        try {
            result_sum += std::stoi(str);
        } catch(std::invalid_argument) {
            std::cout << "������ �ଠ� ��ப�.\n";
            throw;
        }
    }
    return result_sum;
}

double sum_doubles(const std::vector<std::string>& svec) {
    double result_sum = 0;
    for(auto& str : svec) {
        try {
            result_sum += std::stod(str);
        } catch(std::invalid_argument) {
            std::cout << "������ �ଠ� ��ப�.\n";
            throw;
        }
    }
    return result_sum;
}

int main(int /*argc*/, char* /*argv*/ []) {
    std::vector<std::string> svec1 = {"1", "2", "3", "5", "8"};
    std::cout << sum_ints(svec1) << "\n";
    std::vector<std::string> svec2 = {"1.3", "2.3", "3.4", "5.5", "8.6"};
    std::cout << sum_doubles(svec2) << "\n";
    return 0;
}
