#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::cin;
using std::endl;

int main() {
  string hexdidigts{"0123456789ABCDEF"};
  decltype(hexdidigts.size()) n;
  string result;

  while (cin >> n)
    if (n < hexdidigts.size()) result += hexdidigts[n];
  cout << result << endl;
  return 0;
}
