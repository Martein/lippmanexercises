#include <iostream>

class A {
public:
	A() { std::cout << "A()" << std::endl; }
	A(int a, int init_value = 666)
	: m_member(init_value) { std::cout << "A(int a, int init_value = 666)" << std::endl; }

	int member() { return m_member; }

private:
	int m_member;
};

class B : public A {
public:
	using A::A;
	B(int, int) : A(6) { std::cout << "B(int, int)" << std::endl; }
	B() {}
};

int main() {
	B b;
	return 0;
}