#include <iostream>
//------------------------------------------------------------------------------
template <typename T> class MyClass {
private:
  T x;

public:
  explicit MyClass(T value = {}) : x(value) {}
  void Print() const { std::cout << "x = " << x << std::endl; }
};
//------------------------------------------------------------------------------
void foo(std::initializer_list<int> li) {
  int sum = 0;
  for (auto& elem : li) {
    sum += elem;
  }
  std::cout << "Sum: " << sum << std::endl;
}
//------------------------------------------------------------------------------
int main() {
  MyClass<int> myClass;
  myClass.Print();
  foo({ 1, 2, 3 });
  return 0;
}
//------------------------------------------------------------------------------