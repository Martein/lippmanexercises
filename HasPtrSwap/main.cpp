#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

class HasPtr {
    friend void swap(HasPtr& lhs, HasPtr& rhs);

public:
    explicit HasPtr(const std::string& s = std::string())
        : ps(new std::string(s))
        , i(0)
        , use(new size_t(1)) {}
    HasPtr(const HasPtr& p)
        : ps(p.ps)
        , i(p.i)
        , use(p.use) {
        ++*use;
    }

    ~HasPtr() {
        if (--*use == 0) {
            delete ps;
            delete use;
        }
    }

    HasPtr& operator=(HasPtr rhs) {
        ++*rhs.use;
        if (--*use == 0) {
            delete ps;
            delete use;
        }
        swap(*this, rhs);
        return *this;
    }

    bool operator <(HasPtr& rhs) {
        std::cout << *(this->getPs()) << " < " << *(rhs.getPs()) << std::endl;
        return *ps < *rhs.ps;
    }

    inline std::string *getPs() const { return ps; }

private:
    std::string* ps;
    size_t       i;
    std::size_t* use;
};

void swap(HasPtr& lhs, HasPtr& rhs) {
    std::cout << "Swap: " << *lhs.ps << " <--> " << *rhs.ps << std::endl;
    using std::swap;
    swap(lhs.ps, rhs.ps);
    swap(lhs.i, rhs.i);
}

int main() {
    std::vector<HasPtr> vecHP;
    vecHP.push_back("Hello");
    vecHP.push_back(HasPtr("world!"));
    vecHP.push_back(HasPtr("C++"));
    vecHP.push_back(HasPtr("is"));
    vecHP.push_back(HasPtr("simple!"));
    std::sort(vecHP.begin(), vecHP.end());
    for(auto& k : vecHP) std::cout << *k.getPs() << " ";
    return 0;
}


