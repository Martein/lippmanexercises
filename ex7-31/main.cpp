#include <iostream>

using namespace std;

class Y;

class X {
public:
    X(Y* ptrY = nullptr)
        : m_y(ptrY)
    {
    }

private:
    Y* m_y;
};

class Y {
public:
    Y(X valX)
        : m_x(valX)
    {
    }

private:
    X m_x;
};

int main(int argc, char* argv[])
{
    Y y;
    X x(&y);
    return 0;
}
