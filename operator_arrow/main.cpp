#include <iostream>
#include <string>

template <long T>
int foo() {}

template <int N>
class MyClass {
public:
    MyClass<N - 1> operator->() {
        std::cout << "MyClass -> ";
        return MyClass<N - 1>();
    }

private:
    static int m_value;
};

template <>
class MyClass<0> {
public:
    struct answer {
        void print() { std::cout << "print"; }
    };
    answer* operator->() { return nullptr; }
};

int main() {
    MyClass<10> m;
    m->print();
}
