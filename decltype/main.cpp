#include <iostream>
#include <cctype>
#include <string>
#include <chrono>

using std::string;
using std::cout;
using std::cin;
using std::endl;

int main() {
  string str{"some string"};
  for (decltype(str.size()) index = 0;
       index != str.size() && !isspace(str[index]); ++index) {
    str[index] = toupper(str[index]);
  }
  cout << str << endl;
  return 0;
}
