#include <algorithm>
#include <iterator>
#include <cstddef>
#include <memory>  // 
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>
#include <utility>
#include <stdexcept>

using std::runtime_error;
using std::size_t;
using std::shared_ptr;
using std::istringstream;
using std::string;
using std::getline;
using std::vector;
using std::map;
using std::set;
using std::cerr;
using std::cout;
using std::cin;
using std::ostream;
using std::endl;
using std::ifstream;
using std::ispunct;
using std::tolower;
using std::strlen;
using std::pair;

//=============================================================================
class QueryResult {
	friend std::ostream& print(std::ostream&, const QueryResult&);

public:
	typedef std::vector<std::string>::size_type line_no;

	typedef std::set<line_no>::const_iterator line_it;
	
	QueryResult(std::string s,
		std::shared_ptr<std::set<line_no> > p,
		std::shared_ptr<std::vector<std::string> > f) :
		sought(s), lines(p), file(f) { }
	
	std::set<line_no>::size_type size() const {
		return lines->size();
	}

	line_it begin() const {
		return lines->begin();
	}

	line_it end() const {
		return lines->end();
	}

	std::shared_ptr<std::vector<std::string> > get_file() {
		return file;
	}

private:
	std::string sought;  // word this query represents
	std::shared_ptr<std::set<line_no> > lines; // lines it's on
	std::shared_ptr<std::vector<std::string> > file;  //input file
};

inline
std::string make_plural(size_t ctr, const std::string &word, const std::string &ending)
{
	return (ctr > 1) ? word + ending : word;
}
//=============================================================================
class TextQuery {
public:
	typedef std::vector<std::string>::size_type line_no;
	TextQuery(std::ifstream&);
	QueryResult query(const std::string&) const;
	void display_map();        // debugging aid: print the map
private:
	std::shared_ptr<std::vector<std::string> > file; // input file
														  // maps each word to the set of the lines in which that word appears
	std::map<std::string,
		std::shared_ptr<std::set<line_no> > > wm;

	// canonicalizes text: removes punctuation and makes everything lower case
	static std::string cleanup_str(const std::string&);
};

// because we can't use auto, we'll define typedefs 
// to simplify our code

// type of the lookup map in a TextQuery object
typedef map<string, shared_ptr<set<TextQuery::line_no> > > wmType;

// the iterator type for the map
typedef wmType::const_iterator wmIter;

// type for the set that holds the line numbers
typedef shared_ptr<set<TextQuery::line_no> > lineType;

// iterator into the set
typedef set<TextQuery::line_no>::const_iterator lineIter;

// read the input file and build the map of lines to line numbers
TextQuery::TextQuery(ifstream &is) : file(new vector<string>)
{
	string text;
	while (getline(is, text)) {       // for each line in the file
		file->push_back(text);        // remember this line of text
		int n = file->size() - 1;     // the current line number
		istringstream line(text);     // separate the line into words
		string word;
		while (line >> word) {        // for each word in that line
			word = cleanup_str(word);
			// if word isn't already in wm, subscripting adds a new entry
			lineType &lines = wm[word]; // lines is a shared_ptr 
			if (!lines) // that pointer is null the first time we see word
				lines.reset(new set<line_no>); // allocate a new set
			lines->insert(n);      // insert this line number
		}
	}
}

// not covered in the book -- cleanup_str removes
// punctuation and converts all text to lowercase so that
// the queries operate in a case insensitive manner
string TextQuery::cleanup_str(const string &word)
{
	string ret;
	for (string::const_iterator it = word.begin();
		it != word.end(); ++it) {
		if (!ispunct(*it))
			ret += tolower(*it);
	}
	return ret;
}

QueryResult TextQuery::query(const std::string& sought) const
{
	// we'll return a pointer to this set if we don't find sought
	static lineType nodata(new set<line_no>);

	// use find and not a subscript to avoid adding words to wm!
	wmIter loc = wm.find(cleanup_str(sought));

	if (loc == wm.end())
		return QueryResult(sought, nodata, file);  // not found
	else
		return QueryResult(sought, loc->second, file);
}

typedef std::set<TextQuery::line_no>::const_iterator lineIter;

ostream &print(ostream & os, const QueryResult &qr)
{
	// if the word was found, print the count and all occurrences
	os << qr.sought << " occurs " << qr.lines->size() << " "
		<< make_plural(qr.lines->size(), "time", "s") << endl;

	// print each line in which the word appeared
	// for every element in the set 
	for (lineIter num = qr.lines->begin();
		num != qr.lines->end(); ++num)
		// don't confound the user with text lines starting at 0
		os << "\t(line " << *num + 1 << ") "
		<< *(qr.file->begin() + *num) << endl;
	return os;
}

// debugging routine, not covered in the book
void TextQuery::display_map()
{
	wmIter iter = wm.begin(), iter_end = wm.end();

	// for each word in the map
	for (; iter != iter_end; ++iter) {
		cout << "word: " << iter->first << " {";

		// fetch location vector as a const reference to avoid copying it
		lineType text_locs = iter->second;
		lineIter loc_iter = text_locs->begin(),
			loc_iter_end = text_locs->end();

		// print all line numbers for this word
		while (loc_iter != loc_iter_end)
		{
			cout << *loc_iter;

			if (++loc_iter != loc_iter_end)
				cout << ", ";

		}

		cout << "}\n";  // end list of output this word
	}
	cout << endl;  // finished printing entire map
}
//=============================================================================
class Query_base {
	friend class Query;
protected:
	using line_no = TextQuery::line_no;
	~Query_base() = default;
private:
	virtual QueryResult eval(const TextQuery& t) const = 0;
	virtual std::string rep() const = 0;
};
//=============================================================================
class WordQuery : public Query_base {
	friend class Query;

	WordQuery(const std::string& s) : query_word(s) {}

	QueryResult eval(const TextQuery& t) const override {
		return t.query(query_word);
	}

	std::string rep() const {
		return query_word;
	}

	std::string query_word;
};
//=============================================================================
class Query {
	friend Query operator~(const Query&);
	friend Query operator|(const Query&, const Query&);
	friend Query operator&(const Query&, const Query&);
public:
	Query(const std::string& s) : q(new WordQuery(s)) {}

	QueryResult eval(const TextQuery& t) const {
		return q->eval(t);
	}

	const std::string& rep() const {
		return q->rep();
	}

private:
	Query(std::shared_ptr<Query_base> query) : q(query) {}
	std::shared_ptr<Query_base> q;
};
//=============================================================================
class NotQuery : public Query_base {
	friend Query operator~(const Query& operand);

	NotQuery(const Query& q) : query(q) {}
	std::string rep() const override {
		return "~(" + query.rep() + ")";
	}

	QueryResult eval(const TextQuery& t) const {

		auto result = query.eval(t);
		auto ret_lines = std::make_shared<set<line_no>>();
		auto beg = result.begin(); auto end = result.end();
		size_t sz = result.get_file()->size();
		for (size_t n = 0; n != sz; ++n) {
			if (beg == end || *beg != n) {
				ret_lines->insert(n);
			}
			else if (beg != end) {
				++beg;
			}
		}

		return QueryResult(rep(), ret_lines, result.get_file());
	}

	Query query;
};
//=============================================================================
Query operator~(const Query& operand) {
	return std::shared_ptr<Query_base>(new NotQuery(operand));
}
//=============================================================================
class BinaryClass : public Query_base {
protected:
	BinaryClass(const Query& l, const Query& r, std::string s)
		: lhs(l)
		, rhs(r)
		, opSym(s) {}
	std::string rep() const override {
		return "(" + lhs.rep() + " " + opSym + " " + rhs.rep() + ")";
	}
	Query lhs, rhs;
	std::string opSym;
};
//=============================================================================
class AndQuery : public BinaryClass {
	friend Query operator&(const Query&, const Query&);
	AndQuery(const Query& left, const Query& right)
		: BinaryClass(left, right, "&") {}
	QueryResult eval(const TextQuery& t) const override {
		auto left = lhs.eval(t); auto right = rhs.eval(t);
		auto ret_lines = std::make_shared<set<line_no>>();
		std::set_intersection(left.begin(), left.end(),
							  right.begin(), right.end(),
							  std::inserter(*ret_lines, ret_lines->begin()));
		return QueryResult(rep(), ret_lines, left.get_file());
	}
};
//=============================================================================
Query operator&(const Query& lhs, const Query& rhs) {
	return std::shared_ptr<Query_base>(new AndQuery(lhs, rhs));
}
//=============================================================================
class OrQuery : public BinaryClass {
	friend Query operator|(const Query&, const Query&);
	OrQuery(const Query& left, const Query& right)
		:  BinaryClass(left, right, "|")
	{}
	QueryResult eval(const TextQuery& t) const override {
		auto right = rhs.eval(t); auto left = lhs.eval(t);
		auto ret_lines = std::make_shared<set<line_no>>(left.begin(), right.end());
		ret_lines->insert(right.begin(), right.end());
		return QueryResult(rep(), ret_lines, left.get_file());
	}
};
//=============================================================================
Query operator|(const Query& lhs, const Query& rhs) {
	return std::shared_ptr<Query_base>(new OrQuery(lhs, rhs));
}
//=============================================================================
std::ostream& operator<<(std::ostream& os, const Query& q) {
	return os << q.rep();
}
//=============================================================================
// these functions are declared in Query.h
TextQuery get_file(int argc, char **argv)
{
	// get a file to read from which user will query words
	ifstream infile;
	if (argc == 2)
		infile.open(argv[1]);
	if (!infile) {
		throw runtime_error("No input file!");
	}

	return TextQuery(infile);  // builds query map
}
//=============================================================================
bool get_word(string &s1)
{
	cout << "enter a word to search for, or q to quit: ";
	cin >> s1;
	if (!cin || s1 == "q") return false;
	else return true;
}
//=============================================================================
bool get_words(string &s1, string &s2)
{

	// iterate with the user: prompt for a word to find and print results
	cout << "enter two words to search for, or q to quit: ";
	cin >> s1;

	// stop if hit eof on input or a "q" is entered
	if (!cin || s1 == "q") return false;
	cin >> s2;
	return true;
}
//=============================================================================
int main(int argc, char** argv) {
	// gets file to read and builds map to support queries
	TextQuery file = get_file(argc, argv);

	// iterate with the user: prompt for a word to find and print results
	while (true) {
		string sought1, sought2, sought3;
		if (!get_words(sought1, sought2)) break;
		cout << "\nenter third word: ";
		cin >> sought3;
		// find all the occurrences of the requested string
		Query q = Query(sought1) & Query(sought2)
			| Query(sought3);
		cout << "\nExecuting Query for: " << q << endl;
		const QueryResult results = q.eval(file);
		// report matches
		print(cout, results);
	}
	return 0;
}
//=============================================================================