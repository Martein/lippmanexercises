#include <iostream>
#include <string>

int main(int /*argc*/, char */*argv*/[])
{
    std::string s = {"Hello, world! "}, s1 = {"C++ is a very simple language!"};
    s.append(s1, 0, s1.length());
    std::cout << s << std::endl;
    return 0;
}
