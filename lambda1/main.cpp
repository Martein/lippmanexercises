#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>

char op_increase(char c) {
    return tolower(c);
}

int main() {
    std::fstream file(
    "data.txt", std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    std::istreambuf_iterator<char> isit_file(file), eos;
    std::ostreambuf_iterator<char> osit_file(file);
    while(isit_file != eos) {
        char c;
        c  = tolower(*isit_file);
        osit_file = isit_file;
    }
    return 0;
}
