#include <iostream>

using namespace std;

int main() {
  cout << sizeof(char) << endl;
  cout << sizeof(short) << endl;
  cout << sizeof(int) << endl;
  cout << sizeof(long) << endl;
  cout << sizeof(long long) << endl;
  int x[10];
  int* p = x;
  cout << sizeof(x) / sizeof(*x) << endl;
  cout << sizeof(p) / sizeof(*p) << endl;
  return 0;
}
