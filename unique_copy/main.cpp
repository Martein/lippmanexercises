#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

int main(int /*argc*/, char* /*argv*/ []) {
    std::vector<int> lst1 = {2, 4, 5, 5, 3, 2, 4, 3};
    std::vector<int> lst2;
    std::vector<int> lst3 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::list<int> lst4, lst5, lst6;


    std::sort(lst1.begin(), lst1.end());
    std::unique_copy(lst1.cbegin(), lst1.cend(), std::back_inserter(lst2));


    std::copy(lst3.begin(), lst3.end(), std::back_inserter(lst4));
    std::copy(lst3.begin(), lst3.end(), std::front_inserter(lst5));
    std::copy(lst3.begin(), lst3.end(), std::inserter(lst6, lst6.begin()));

    for (auto& v : lst6)
        std::cout << v << " ";

    return 0;
}
