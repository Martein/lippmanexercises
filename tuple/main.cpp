#include <iostream>
#include <tuple>
#include <vector>

int main() {

    int input = 0;
    std::cin >> input;
    std::tuple<int, char, std::string> t = { 6, 'd', "hello" };
    using t_type                         = std::tuple_element<2, decltype(t)>::type;
    std::vector<t_type> tvec(std::get<0>(t), std::get<2>(t));
    tvec.insert(begin(tvec) + 2, std::string("world"));
    for (auto& v : tvec)
        std::cout << v.c_str() << std::endl;
    std::tuple<size_t, size_t, size_t> threeD = { 1, 2, 3 };
    return 0;
}
