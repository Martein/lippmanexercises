#include <iostream>

void foo(int* a, int* b) {
  ++(*a);
  --(*b);
}

int main() {
  int i1 = 1, i2 = 2;
  foo(&i1, &i2);
  std::cout << "i1 = " << i1 << " i2 = " << i2 << std::endl;
  return 0;
}
