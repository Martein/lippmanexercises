#include <iostream>
#include <string>

using namespace std;

int main() {
  cout << "Enter string: ";
  string str;
  size_t aCnt = 0, eCnt = 0, iCnt = 0, oCnt = 0, enterCnt = 0, tabsCnt = 0,
         spacesCnt = 0, ffCnt = 0, flCnt = 0, fiCnt = 0;
  while (getline(cin, str)) {
    for (string::size_type i = 0; i < str.size(); ++i) {
      auto ch = str[i];
      if (ch == 'a' || ch == 'A') {
        ++aCnt;
      } else if (ch == 'e' || ch == 'E') {
        ++eCnt;
      } else if (ch == 'i' || ch == 'I') {
        ++iCnt;
      } else if (ch == 'o' || ch == 'O') {
        ++oCnt;
      } else if (ch == '\n') {
        ++enterCnt;
      } else if (ch == '\t') {
        ++tabsCnt;
      } else if (ch == ' ') {
        ++spacesCnt;
      } else if (ch == 'f' && i != (str.size() - 1)) {
        if (str[i + 1] == 'f') {
          ++ffCnt;
        } else if (str[i + 1] == 'l') {
          ++flCnt;
        } else if (str[i + 1] == 'i') {
          ++fiCnt;
        }
      }
    }
  }
  cout << "Number of \'a\' letters: " << aCnt << endl;
  cout << "Number of \'e\' letters: " << eCnt << endl;
  cout << "Number of \'i\' letters: " << iCnt << endl;
  cout << "Number of \'o\' letters: " << oCnt << endl;
  cout << "Number of spaces: " << spacesCnt << endl;
  cout << "Number of tabs: " << tabsCnt << endl;
  cout << "Number of enters: " << enterCnt << endl;
  cout << "Number of ff's: " << ffCnt << endl;
  cout << "Number of fl's: " << flCnt << endl;
  cout << "Number of fi's: " << fiCnt << endl;
  return 0;
}
