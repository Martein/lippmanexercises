#include <iostream>
#include <vector>

using std::vector;
using std::cout;
using std::cin;
using std::endl;

int get_size() {
  int n;
  cin >> n;
  return n;
}

int main() {
  char ch[get_size()];
  cout << sizeof(ch) << endl;
  unsigned grade;
  vector<unsigned> scores(11, 0);
  while (cin >> grade) {
    if (grade <= 100) ++scores[grade / 10];
  }
  for (auto it = scores.begin(); it != scores.end(); ++it)
    cout << *it << " ";
  return 0;
}
