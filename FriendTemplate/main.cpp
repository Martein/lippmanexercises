#include <iostream>

using namespace std;

template <typename T> class TestClass;
class A {
public:
	void foo(TestClass<char>& t) {
		t.m_value = 6;
	}
};

class B {
public:
	void foo() {

	}
};

class C {
public:
};

template <typename T> class TestClass {
	friend class A;
public:

private:
	int m_value;
};

int main() {
	A<char> ai;
	TestClass<char> ti;
	ai.foo(ti);
	return 0;
}