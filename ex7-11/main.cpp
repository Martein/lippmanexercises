#include <iostream>
#include <string>

struct Sales_data {
  Sales_data() = default;
  Sales_data(const std::string &s) : bookNo(s) {}
  Sales_data(const std::string &s, unsigned n, double p)
      : bookNo(s), units_sold(n), revenue(p * n) {}
  Sales_data(std::istream &is) { read(is, *this); }
  double avg_price() const;
  Sales_data &combine(const Sales_data &rhs);
  Sales_data add(const Sales_data &lhs, const Sales_data &rhs);
  std::string isbn() const { return bookNo; }
  std::istream &read(std::istream &is, Sales_data &item);
  std::ostream &print(std::ostream &os, const Sales_data &item);

private:
  std::string bookNo = {};
  unsigned units_sold = {};
  double revenue = {};
};

double Sales_data::avg_price() const {
  if (units_sold)
    return revenue / units_sold;
  else
    return 0;
}

Sales_data &Sales_data::combine(const Sales_data &rhs) {
  units_sold += rhs.units_sold;
  revenue += rhs.revenue;
  return *this;
}

Sales_data Sales_data::add(const Sales_data &lhs, const Sales_data &rhs) {
  Sales_data sum = lhs;
  sum.combine(rhs);
  return sum;
}

std::istream &Sales_data::read(std::istream &is, Sales_data &item) {
  double price = 0;
  is >> item.bookNo >> item.units_sold >> price;
  item.revenue = price * item.units_sold;
  return is;
}

std::ostream &Sales_data::print(std::ostream &os, const Sales_data &item) {
  os << item.isbn() << " " << item.units_sold << " " << item.revenue << " "
     << item.avg_price();
  return os;
}

int main() {
  Sales_data total(std::cin);
  Sales_data trans;
  while (trans.read(std::cin, trans)) {
    if (total.isbn() == trans.isbn()) {
      total.combine(trans);
    } else {
      total.print(std::cout, total);
      total = trans;
    }
  }
  std::cout << "No data." << std::endl;
  return 0;
}
