#include <iostream>
#include <vector>
#include <QApplication>
#include <QDebug>
//#include "Screen.h"

class NoDefault {
public:
    NoDefault(std::string) {}
    NoDefault(int) {}
private:
    struct A {
    };
};

//struct A {
//    NoDefault my_mem;
//};

//class B {
//public:
//    B() {}

//private:
//    NoDefault b_member;
//};


void foo(NoDefault nd) {
    std::cout << "Вызвана функция foo();" << std::endl;
}

class C {
public:
    C()
        : c_member(5)
    {
    }

private:
    NoDefault c_member;
};

void foo()
{
}

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QLocale::system().name();
    QString locale = QLocale::system().name();
    qDebug() << locale;
    return app.exec();
}
