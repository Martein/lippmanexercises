TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += qt
QT += core gui widgets

SOURCES += main.cpp

HEADERS += \
    Screen.h \
    Window_mgr.h
