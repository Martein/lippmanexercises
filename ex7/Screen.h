#ifndef SCREEN_H
#define SCREEN_H

#include <string>

class Screen {
    friend class Window_mgr;
public:
    typedef std::string::size_type pos;
    Screen() = default;
    Screen(size_t h, size_t w);
    Screen(size_t h, size_t w, char s);
    Screen& move(pos r, pos c);
    char get(pos r, pos c);
    char get();
    void some_member();

private:
    pos cursor = 0;
    pos height = 0, width = 0;
    std::string contents;
    mutable size_t access_ctr;
};

Screen::Screen(size_t h, size_t w)
    : height(h)
    , width(w)
    , contents(h * w, ' ')
{
}

Screen::Screen(size_t h, size_t w, char s)
    : height(h)
    , width(w)
    , contents(h * w, s)
{
}

inline Screen& Screen::move(Screen::pos r, Screen::pos c)
{
    pos row = width * r;
    cursor = row + c;
    return *this;
}

inline char Screen::get(Screen::pos r, Screen::pos c)
{
    pos row = r * width;
    return contents[row + c];
}

char Screen::get() { return contents[cursor]; }

void Screen::some_member()
{
    ++access_ctr;
}

#endif // SCREEN_H
