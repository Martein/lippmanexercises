#include <iostream>
#include <vector>

void outputSizeAndCapacity(std::vector<int>& vec)
{
    std::cout << "vector размер: " << vec.size()
              << " vector capacity: " << vec.capacity() << std::endl;
}

int main(int /*argc*/, char */*argv*/[])
{
    std::vector<int> ivec;
    for (std::vector<int>::size_type ix = 0; ix != 24; ++ix)
        ivec.push_back(ix);
    outputSizeAndCapacity(ivec);
    ivec.reserve(50);
    outputSizeAndCapacity(ivec);
    while (ivec.size() != ivec.capacity()) {
        ivec.push_back(0);
    }
    outputSizeAndCapacity(ivec);
    ivec.push_back(1);
    outputSizeAndCapacity(ivec);
    ivec.shrink_to_fit();
    outputSizeAndCapacity(ivec);
    return 0;
}
