TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Sales_data.cpp

HEADERS += \
    Sales_data.h
