#include <iostream>
#include <fstream>
#include <ostream>

#include "Sales_data.h"

int main(int /*argc*/, char** argv)
{
    std::ifstream in(argv[1]);
    std::ofstream out(argv[2], std::ofstream::app);

    if (in.is_open()) {
        if (out.is_open()) {
            Sales_data sd;
            while(read(in, sd)) {
                print(out, sd);
                out << std::endl;
            }
        } else {
            std::cout << "Can't open out file." << std::endl;
        }
    } else {
        std::cout << "Can't open in file." << std::endl;
    }

    return 0;
}
