#include <iostream>
#include <string>
#include <iostream>
#include <string>
#include <set>
#include <map>

using namespace std;

struct Sales_data {
  std::string isbn() const { return bookNo; }
  Sales_data& combine(const Sales_data&);
  double avg_price() const;

  std::string bookNo;
  unsigned units_told = 0;
  double revenue = 0.0;
};

Sales_data add(const Sales_data&, const Sales_data&);
std::ostream& print(std::ostream&, const Sales_data&);
std::istream& read(std::istream&, Sales_data&);

int b;

size_t counter(const string& S) {
  std::map<char, int> m;
  for (auto ch : S) {
    m[ch]++;
  }
  int sum = 0;
  for (auto i : m) {
    ++sum;
  }
  return sum;
}

int main() {
  setlocale(0, "");
  string S;
  getline(cin, S);
  cout << "Различных символов в строке: " << counter(S) << endl;

  system("pause");
  return 0;
  return 0;
}
