#include <iostream>
#include <cassert>

int main() {
  int a, b;
  std::cin >> a >> b;
  assert(a > b);
  return 0;
}
