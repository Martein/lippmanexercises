#include <iostream>
#include <vector>

using namespace std;

int main() {
  vector<int> ivec = { 4, 1, 2, 3, 4, 4, 6, 7, 9 };
  for (auto& i : ivec) {
    i *= (i % 2) ? 2 : 1;
    cout << i << " ";
  }
  return 0;
}