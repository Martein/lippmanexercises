#include <iostream>

using namespace std;

int main(int, char**) {
  int x = 3, y = 3;
  bool someValue = true;
  someValue ? ++x, ++y : --x, --y;
  cout << "x = " << x << " y = " << y << endl;
  return 0;
}
