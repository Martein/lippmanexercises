#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

std::istream& read_istream(std::istream& is)
{
    std::string word;
    while (is >> word) {
        std::cout << word << " ";
    }
    is.clear();
    return is;
}

struct PeoplePhone {
    std::string name;
    std::vector<std::string> phones;
};

int main(int /*argc*/, char* argv[])
{
    std::vector<PeoplePhone> people;
    std::ifstream in_file(argv[1]);

    if (in_file.is_open()) {

        std::vector<std::string> strFileRecords;

        std::string line;
        std::istringstream record;
        while (std::getline(in_file, line)) {
            strFileRecords.push_back(line);
        }

        for (const auto& word : strFileRecords) {
            record.clear();
            record.str(word);

            PeoplePhone info;
            record >> info.name;

            std::string num;
            while (record >> num) {
                info.phones.push_back(num);
            }

            people.push_back(info);
        }

        for (const auto& record : people) {
            std::cout << record.name << " ";
            for (const auto& phone : record.phones) {
                std::cout << phone << " ";
            }
            std::cout << std::endl;
        }
    }
    else {
        std::cerr << "Can't open in_file." << std::endl;
    }
    return 0;
}
