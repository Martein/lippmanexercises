#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>

using std::string;
using std::map;
using std::ifstream;
using std::istringstream;
using std::cout;
using std::endl;
using std::runtime_error;

const string& transform(const string& s, map<string, string> m) {
    auto map_it = m.find(s);

    if (map_it != m.end())
        return map_it->second;

    return s;
}

map<string, string> buildMap(ifstream& map_file) {
    map<string, string> trans_map;
    string key;
    string value;

    while (map_file >> key && std::getline(map_file, value)) {
        if (value.size() > 1)
            //trans_map[key] = value.substr(1);
            trans_map.insert({key, value.substr(1)});
        else
            throw runtime_error("No rule for " + key);
    }

    return trans_map;
}

void foo() {
}

void word_transform(ifstream& map_file, ifstream& input) {
    map<string, string> trans_map = buildMap(map_file);
    string text;

    while (std::getline(input, text)) {
        istringstream stream(text);
        bool          firstspace = true;
        string        word       = {};

        while (stream >> word) {
            if (firstspace)
                firstspace = false;
            else
                cout << " ";

            cout << transform(word, trans_map);
        }

        cout << endl;
    }
}

int main(int argc, char* argv[]) {
    if (argc == 3) {
        ifstream map_file(argv[1]);
        ifstream input(argv[2]);

        if (map_file.is_open() && input.is_open())
            word_transform(map_file, input);
        else
            std::cout << "One of the files isn't opened.\n";
    } else
        std::cout << "Incorrect number of parameters.\n";

    return 0;
}
