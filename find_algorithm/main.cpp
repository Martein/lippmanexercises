#include <algorithm>
#include <iostream>
#include <list>
#include <string>
#include <vector>

using container_int = std::list<int>;

std::size_t count_ints(container_int& ivec, int find_value) {
        return std::count(ivec.begin(), ivec.end(), find_value);
}

int main(int /*argc*/, char */*argv*/[])
{
    container_int ivec;
    int i;
    std::cout << "Подайте на вход ваши числа: ";
    while (std::cin >> i) {
        ivec.push_back(i);
    }
    std::cout << count_ints(ivec, 1);

    return 0;
}
