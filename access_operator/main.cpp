#include <iostream>

using namespace std;

class A {
public:
  A() {}
  A(const A& other) { cout << "copy ctor"; }
  A operator++(int) {
    A temp(*this);
    return temp;
  }
};

A bar() { return A(); }

int main() {
  A a;
  a++;
  return 0; }
