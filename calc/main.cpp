#include <functional>
#include <iostream>
#include <map>
#include <string>

int add(int a, int b) { return a + b; }

class sub {
public:
    int operator()(int a, int b) { return a - b; }
};

int main() {
    int                                                 a;
    std::map<std::string, std::function<int(int, int)>> calc = {
        { "+", add }, { "-", sub() }, { "*", std::multiplies<int>() }
    };

    std::cout << calc["+"](1, 2) << std::endl;
    std::cout << calc["-"](4, 2) << std::endl;
    std::cout << calc["*"](4, 2) << std::endl;

    return 0;
}
