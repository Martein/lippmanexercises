#include <initializer_list>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

class StrBlob2 {
private:
    int m_value;
public:
    StrBlob2() {}
    ~StrBlob2() {}
};

class StrBlob {
public:
    typedef std::vector<std::string>::size_type size_type;
    StrBlob();
    StrBlob(std::initializer_list<std::string> il)
      : data(std::make_shared<std::vector<std::string>>(il)) {}
    size_type size() const;
    void push_back(const std::string& t);
    void               pop_back();
    const std::string& front() const;
    const std::string& back() const;

private:
    std::shared_ptr<std::vector<std::string>> data;
    void check(size_type /*i*/, const std::string& /*msg*/) const;
};

StrBlob::StrBlob() {}

StrBlob::size_type StrBlob::size() const {
    return data->size();
}

void StrBlob::push_back(const std::string& t) {
    data->push_back(t);
}

void StrBlob::pop_back() {
    check(0, "pop_back on empty StrBlob");
    data->pop_back();
}

const std::string& StrBlob::front() const {
    check(0, "front on empty StrBlob");
    return data->front();
}

const std::string& StrBlob::back() const {
    check(0, "back on empty StrBlob");
    return data->back();
}

void StrBlob::check(StrBlob::size_type i, const std::string& msg) const {
    if (i > this->size())
        throw std::out_of_range(msg);
}

class A {
public:
    A() { std::cout << "Ура! Я родился!" << std::endl; }
    ~A() { std::cout << "Увы! Я умер!" << std::endl; }
};

void foo() {
    A* pa = new A;
    // throw "Yea!";
    delete pa;
    return;
}

int main(int /*argc*/, char* /*argv*/ []) {
    StrBlob b1 = {"at", "an", "the"};
    StrBlob2 b3;
    auto    b2 = b1;
    std::cout << b2.back() << std::endl;
    b1.push_back("about");
    std::cout << b2.back() << std::endl;
    try {
        foo();
    } catch (...) {}
    std::cout << "Buy!" << std::endl;

    return 0;
}
