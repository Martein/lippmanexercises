#include <iostream>
#include <bitset>
#include <regex>

using namespace std;

int main()
{
    regex reg("\\d+[,.]*\\d*\\s*(��|ru|�|k)(\\b|$|s*)([^a-zA-Z�-�-�0-9��]|$)");
    string test_str = "���� �����ࠤ��� 100�";
    smatch result;
    if (regex_search(test_str, result, reg)) {
        cout << result.str() << endl;
    }
    return 0;
}
