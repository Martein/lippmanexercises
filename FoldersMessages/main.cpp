#include <iostream>
#include <set>
#include <string>

class Folder;
//------------------------------------------------------------------------------
class Message {
    friend class Folder;

public:
    Message() {}
    Message(const Message& m);
    Message& operator=(const Message&) { return *this; }
    void     save(Folder& f);
    void     remove(Folder& f);
    ~Message();

private:
    void add_to_Folders(const Message&);
    void remove_from_Folders();

    std::string       contents;
    std::set<Folder*> m_folders;
};
//------------------------------------------------------------------------------
class Folder {
public:
    Folder() {}
    void addMsg(Message* msg);
    void remMsg(Message* msg);

private:
    std::set<Message*> m_messages;
};
void Folder::addMsg(Message* msg) { m_messages.insert(msg); }
//------------------------------------------------------------------------------
void Folder::remMsg(Message* msg) { m_messages.erase(msg); }
//------------------------------------------------------------------------------
Message::Message(const Message& m) { add_to_Folders(m); }
//------------------------------------------------------------------------------
void Message::save(Folder& f) {
    m_folders.insert(&f);
    f.addMsg(this);
}
//------------------------------------------------------------------------------
void Message::remove(Folder& f) {
    m_folders.erase(&f);
    f.remMsg(this);
}
//------------------------------------------------------------------------------
Message::~Message() { remove_from_Folders(); }
//------------------------------------------------------------------------------
void Message::add_to_Folders(const Message& m) {
    for (auto f : m.m_folders) {
        f->addMsg(this);
    }
}
//------------------------------------------------------------------------------
void Message::remove_from_Folders() {
    for (auto f : m_folders) {
        f->remMsg(this);
    }
}
//------------------------------------------------------------------------------
int main() {
    std::cout << "Hello World!" << std::endl;
    return 0;
}
//------------------------------------------------------------------------------
