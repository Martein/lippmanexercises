#include <iostream>
#include <vector>
#include <list>

using namespace std;

int main(int /*argc*/, char */*argv*/[])
{
    vector<int> ivec = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    list<int> ilist(ivec.cbegin, ivec.cend() - 1);
    for (auto v : ilist) {
        cout << v << " " << endl;
    }
    return 0;
}
